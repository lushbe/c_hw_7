#include <iostream>

class Stack
{
public:
    int A;
    class Stack* next;                      
};

void push(Stack*& NEXT, const int VALUE)
{
    Stack* MyStack = new Stack;             
    MyStack->A = VALUE;                 
    MyStack->next = NEXT;                   
    NEXT = MyStack;                         
}

int pop(Stack*& NEXT)
{
    int temp = NEXT->A;                 
    Stack* MyStack = NEXT;                   
                                            
    NEXT = NEXT->next;                      
    delete MyStack;                         
    std::cout << temp;                      
    return temp;                            
}

int main()
{
    Stack* p = 0;

    push(p, 666);                            
    push(p, 999);                            

    pop(p);
    std::cout << '\n';
    pop(p);                                 

   
}